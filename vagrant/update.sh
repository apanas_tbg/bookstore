#!/bin/bash 

service mysql stop
service mysql start
mysql -uroot -ptest </vagrant/vagrant/database/db_changes.sql

service apache2 stop
service tomcat7 stop
mvn -f ../pom.xml clean install
service tomcat7 start
service apache2 start
