libraryApp.controller("BuyController", function ($scope, $http) {
    $scope.booksforsale = {};
    $scope.bought = {};
    $scope.viewBooksForSale = false;
	$scope.viewBoughtBooks = false;
    $scope.response = {};
    $scope.data = {};
	$scope.showOrderButton = false;
    $scope.showBuyMsg = false;
    $scope.bookId;

    $scope.buyBooks = function(userId) {
        console.log("buyBooks works, userId: " + userId);
        $scope.userId = userId;
        if ($scope.viewBooksForSale) {
            $scope.viewBooksForSale = false;
            $scope.showBuyMsg = false;
        } else {
            $scope.viewBooksForSale = true;
        }

        if ($scope.viewBooksForSale) {
            $http.get("/bookforsale")
                .then(function (response) {
                    $scope.booksforsale = response.data.content;
                    console.log($scope.booksforsale);
                }, function (response) {
                });
        }
    };
    
    $scope.getBoughtBooks = function(userId) {
        console.log("getBoughtBooks works, userId: " + userId);
        $scope.userId = userId;
        if ($scope.viewBoughtBooks) {
            $scope.viewBoughtBooks = false;
            $scope.showBuyMsg = false;
        } else {
            $scope.viewBoughtBooks = true;
        }

        if ($scope.viewBoughtBooks) {
            $http.get("/bought?user_id="+userId+"")
                .then(function (response) {
                    $scope.bought = response.data.content;
                    console.log($scope.bought);
                }, function (response) {});
        }
    }
    
    $scope.submitBuy = function (bookId, userId) {
        $scope.bookId = bookId;
        $scope.data = {
            "userId" : userId,
            "bookId" : bookId
        };
        
        $http.post("/bought", $scope.data)
            .then(function (response) {
                $scope.response = response.data;
                $scope.showBuyMsg = true;
				$scope.showOrderButton = false;

                if (response.data.success == false) {
                    $scope.showOrderButton = true;
                }

                console.log("buy post request success!");
            }, function (response) {});
    };

    $scope.order = function(userId, bookId) {
        console.log("order works, userId: " + userId);
        $scope.data = {
            "userId" : userId,
            "bookId" : bookId
        }

        $http.post("/book/order", $scope.data)
            .then(function (response) {
                $scope.response = response.data;
                $scope.showOrderButton = false;
                
                console.log("order post request success!");
            }, function (response) {});
    };
});
