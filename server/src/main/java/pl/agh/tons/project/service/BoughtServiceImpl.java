package pl.agh.tons.project.service;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import pl.agh.tons.project.dao.abstraction.CopyForSaleDao;
import pl.agh.tons.project.dao.abstraction.BoughtDao;
import pl.agh.tons.project.dao.abstraction.UserDao;
import pl.agh.tons.project.model.CopyForSale;
import pl.agh.tons.project.model.Bought;
import pl.agh.tons.project.model.User;

import java.util.Date;
import java.util.List;

public class BoughtServiceImpl implements BoughtService {

    private BoughtDao boughtDao;

    private CopyForSaleDao copyForSaleDao;

    private UserDao userDao;

    @Inject
    public BoughtServiceImpl(BoughtDao boughtDao, CopyForSaleDao copyForSaleDao, UserDao userDao) {
        this.boughtDao = boughtDao;
        this.copyForSaleDao = copyForSaleDao;
        this.userDao = userDao;
    }

    @Override
    public List<Bought> getBought(int userId) {
        return boughtDao.getByForeignKey("user_id", userId);
    }

    @Override
    @Transactional
    public boolean buyBook(int bookId, int userId) {
        List<CopyForSale> copies = copyForSaleDao.getNotSoldCopies(bookId);
        if (copies.isEmpty()) {
            return false;
        }

        CopyForSale copyForSale = copies.get(0);
        copyForSaleDao.setSold(copyForSale);

        User user = userDao.getById(userId);
        CopyForSale copy = copyForSaleDao.getById(bookId);
        Bought newBought = new Bought(user, copy, new Date(), 0);

        boughtDao.addBought(newBought);
        return true;
    }
}
