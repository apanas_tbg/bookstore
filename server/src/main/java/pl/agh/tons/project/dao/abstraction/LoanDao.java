package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Loan;

public interface LoanDao extends Dao<Loan> {
    void addLoan(Loan loan);

    void setLoan(Loan loan);

    Loan getLoan(int copyId, int userId);
}
