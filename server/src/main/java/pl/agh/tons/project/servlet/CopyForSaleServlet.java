package pl.agh.tons.project.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import pl.agh.tons.project.model.CopyForSale;
import pl.agh.tons.project.service.CopyForSaleService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Singleton
public class CopyForSaleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(CopyForSaleServlet.class);

    private CopyForSaleService copyForSaleService;
    private WebProtocol webProtocol;

    @Inject
    public CopyForSaleServlet(CopyForSaleService copyForSaleService, WebProtocol webProtocol) {
        this.copyForSaleService = copyForSaleService;
        this.webProtocol = webProtocol;
    }

    protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws IOException, ServletException {

        httpResponse.setContentType("application/json");
        httpResponse.setCharacterEncoding("UTF-8");

        List<CopyForSale> copies = copyForSaleService.getAllNotSoldBooks();

        Response<CopyForSale> response = new Response(copies);
        response.setMsg("Wszystkie kopie ksiazek dostepne do kupienia.");
        response.setSuccess(true);
        httpResponse.getWriter().write(webProtocol.prepareResponse(response));
    }

    protected void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws IOException, ServletException {


    }
}
