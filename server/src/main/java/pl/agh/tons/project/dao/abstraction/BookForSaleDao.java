package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.BookForSale;

public interface BookForSaleDao extends Dao<BookForSale> {
}
