package pl.agh.tons.project.service;

public interface OrdersService {

    void orderBook(int userId, int bookId);

    void cancelOrder(int userId, int bookId);
}
