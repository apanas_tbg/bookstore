package pl.agh.tons.project.model;

import javax.persistence.*;

@Entity
@Table(name="copyforsale")
public class CopyForSale {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id", length=11, unique=true)
    private int id;

    @ManyToOne
    @JoinColumn(name="bookforsale_id", nullable=false)
    private BookForSale bookForSale;

    @Column(name="sold", nullable=false)
    private int sold;

    public CopyForSale() {}

    public CopyForSale(BookForSale bookForSale) {
        this.bookForSale = bookForSale;
    }

    public CopyForSale(BookForSale bookForSale, int sold) {
        this.bookForSale = bookForSale;
        this.sold = sold;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookForSale getBook() {
        return bookForSale;
    }

    public void setBook(BookForSale bookForSale) {
        this.bookForSale = bookForSale;
    }
}
