package pl.agh.tons.project.service;

import pl.agh.tons.project.model.CopyForSale;

import java.util.List;

public interface CopyForSaleService {
    List<CopyForSale> getAllNotSoldBooks();
}
