package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.User;

public interface UserDao extends Dao<User> {

    User getByEmailAndPassword(String email, String password);

}
