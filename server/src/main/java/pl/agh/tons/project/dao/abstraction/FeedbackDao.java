package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Feedback;

public interface FeedbackDao extends Dao<Feedback> {
}
