package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Category;

public interface CategoryDao extends Dao<Category> {
}
