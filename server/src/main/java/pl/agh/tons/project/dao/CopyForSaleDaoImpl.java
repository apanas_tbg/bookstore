package pl.agh.tons.project.dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import pl.agh.tons.project.dao.abstraction.AbstractDao;
import pl.agh.tons.project.dao.abstraction.CopyForSaleDao;
import pl.agh.tons.project.model.CopyForSale;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class CopyForSaleDaoImpl extends AbstractDao<CopyForSale> implements CopyForSaleDao {

    @Inject
    public CopyForSaleDaoImpl(Provider<EntityManager> entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public List<CopyForSale> getAllNotSoldCopies() {
        Query query = entityManagerFactory.get().createQuery("from Copy WHERE rented=0");

        return (List<CopyForSale>) query.getResultList();
    }

    @Override
    public void setSold(CopyForSale copyForSale) {
        copyForSale.setSold(1);

        entityManagerFactory.get().<CopyForSale>merge(copyForSale);
    }

    @Override
    public List<CopyForSale> getNotSoldCopies(int bookId) {
        Query query = entityManagerFactory.get().createQuery("from CopyForSale WHERE sold=0 AND bookforsale_id = :bookId");
        query.setParameter("bookId", bookId);

        return (List<CopyForSale>) query.getResultList();
    }

    @Override
    public List<CopyForSale> getNotSoldCopies(List<Integer> bookIds) {
        StringBuilder sql = new StringBuilder("from CopyForSale WHERE ");
        String or = "";
        for (int i=0; i<bookIds.size(); i++) {
            sql.append(or);
            sql.append("bookforsale_id = :bookId" + i);
            or = " OR ";
        }

        Query query = entityManagerFactory.get().createQuery(sql.toString());
        for (int i=0; i<bookIds.size(); i++) {
            query.setParameter("bookId"+i, bookIds.get(i));
        }

        return (List<CopyForSale>) query.getResultList();
    }

    @Override
    public void setCopyForSale(CopyForSale copyForSale) {
        entityManagerFactory.get().merge(copyForSale);
    }
}
