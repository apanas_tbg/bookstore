package pl.agh.tons.project.service;

import com.google.inject.Inject;
import pl.agh.tons.project.dao.abstraction.CopyForSaleDao;
import pl.agh.tons.project.model.CopyForSale;

import java.util.List;

public class CopyForSaleServiceImpl implements CopyForSaleService {

    private CopyForSaleDao copyForSaleDao;

    @Inject
    public CopyForSaleServiceImpl(CopyForSaleDao copyForSaleDao) {
        this.copyForSaleDao = copyForSaleDao;
    }


    @Override
    public List<CopyForSale> getAllNotSoldBooks() {
        return copyForSaleDao.getAllNotSoldCopies();
    }
}
