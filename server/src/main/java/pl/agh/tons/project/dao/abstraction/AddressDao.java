package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Address;

public interface AddressDao extends Dao<Address> {
}
