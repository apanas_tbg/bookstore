package pl.agh.tons.project.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import pl.agh.tons.project.service.OrdersService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Singleton
public class OrdersServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(OrdersServlet.class);

    private OrdersService ordersService;
    private WebProtocol webProtocol;

    @Inject
    public OrdersServlet(OrdersService ordersService, WebProtocol webProtocol) {
        this.ordersService = ordersService;
        this.webProtocol = webProtocol;
    }

    protected void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws IOException, ServletException {

        Map<String, Object> requestMap = webProtocol.prepareRequest(httpRequest);

        httpResponse.setContentType("application/json");
        httpResponse.setCharacterEncoding("UTF-8");

        int bookId = Integer.valueOf((String) requestMap.get("bookId"));
        int userId = ((Double) requestMap.get("userId")).intValue();

        ordersService.orderBook(userId, bookId);

        Response response = new Response();
        response.setSuccess(true);
        response.setMsg("Zamowienie na ksiazke zostalo przyjeta!");

        httpResponse.getWriter().write(webProtocol.prepareResponse(response));
    }
}
