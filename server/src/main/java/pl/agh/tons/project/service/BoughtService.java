package pl.agh.tons.project.service;

import pl.agh.tons.project.model.Bought;

import java.util.List;

public interface BoughtService {

    List<Bought> getBought(int userId);

    boolean buyBook(int copyId, int userId);

}
