package pl.agh.tons.project.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="orders")
public class Orders implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @ManyToOne
    @JoinColumn(name="bookforsale_id", nullable=false)
    private BookForSale bookForSale;

    public Orders() {}

    public Orders(User user, BookForSale bookForSale) {
        this.user = user;
        this.bookForSale = bookForSale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BookForSale getBookForSale() {
        return bookForSale;
    }

    public void setBookForSale(BookForSale bookForSale) {
        this.bookForSale = bookForSale;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Orders orders = (Orders) o;

        if (id != orders.id) return false;
        if (user.getId() != orders.user.getId()) return false;
        if (bookForSale.getId() != orders.bookForSale.getId() ) return false;        

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + user.hashCode();
        result = 31 * result + bookForSale.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Ordersword{" +
                "id=" + id +
                ", user_id=" + user.getId() +
                ", bookforsale_id=" + bookForSale.getId() +
                '}';
    }

}