package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.CopyForSale;

import java.util.List;

public interface CopyForSaleDao extends Dao<CopyForSale> {
    List<CopyForSale> getAllNotSoldCopies();

    void setSold(CopyForSale copyForSale);

    List<CopyForSale> getNotSoldCopies(int bookId);

    List<CopyForSale> getNotSoldCopies(List<Integer> bookIds);

    void setCopyForSale(CopyForSale copyForSale);
}
