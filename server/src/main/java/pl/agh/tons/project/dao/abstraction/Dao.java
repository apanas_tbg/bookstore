package pl.agh.tons.project.dao.abstraction;

import java.util.List;

public interface Dao<T> {

    T getById(int id);

    List<T> getAll();

    List<T> getByForeignKey(String column, int id);
}
