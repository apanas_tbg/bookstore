package pl.agh.tons.project.service;

import com.google.inject.Inject;
import pl.agh.tons.project.dao.abstraction.CopyDao;
import pl.agh.tons.project.model.Copy;

import java.util.List;

public class CopyServiceImpl implements CopyService {

    private CopyDao copyDao;

    @Inject
    public CopyServiceImpl(CopyDao copyDao) {
        this.copyDao = copyDao;
    }


    @Override
    public List<Copy> getAllNotRentedBooks() {
        return copyDao.getAllNotRentedCopies();
    }
}
