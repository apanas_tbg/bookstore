package pl.agh.tons.project.service;

import pl.agh.tons.project.model.User;

public interface RegisterService {

    void registerUser(User user);

    void removeUser(User user);
}
