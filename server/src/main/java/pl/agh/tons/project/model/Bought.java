package pl.agh.tons.project.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="bought")
public class Bought implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name="copyforsale_id", nullable = false)
    private CopyForSale copyForSale;

    @Temporal(TemporalType.DATE)
    @Column(name="bought_date")
    private Date boughtDate;

    @Column(name="archive", nullable=false)
    private int archive;

    public Bought() {}

    public Bought(Date boughtDate) {
        this.boughtDate = boughtDate;
    }

    public Bought(User user, CopyForSale copyForSale, Date boughtDate) {
        this.user = user;
        this.copyForSale = copyForSale;
        this.boughtDate = boughtDate;
    }

    public Bought(User user, CopyForSale copyForSale, Date boughtDate, int archive) {
        this.user = user;
        this.copyForSale = copyForSale;
        this.boughtDate = boughtDate;
        this.archive = archive;
    }

    public int getArchive() {
        return archive;
    }

    public void setArchive(int archive) {
        this.archive = archive;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CopyForSale getCopyForSale() {
        return copyForSale;
    }

    public void setCopyForSale(CopyForSale copyForSale) {
        this.copyForSale = copyForSale;
    }

    public Date getBoughtDate() {
        return boughtDate;
    }

    public void setBoughtDate(Date boughtDate) {
        this.boughtDate = boughtDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bought bought = (Bought) o;

        if (id != bought.id) return false;
        if (!boughtDate.equals(bought.boughtDate)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + boughtDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Boughtword{" +
                "id=" + id +
                ", boughtDate=" + boughtDate +
                '}';
    }
}
