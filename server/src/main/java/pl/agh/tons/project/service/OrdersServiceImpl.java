package pl.agh.tons.project.service;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;

import pl.agh.tons.project.dao.abstraction.BookForSaleDao;
import pl.agh.tons.project.dao.abstraction.OrdersDao;
import pl.agh.tons.project.dao.abstraction.UserDao;
import pl.agh.tons.project.model.BookForSale;
import pl.agh.tons.project.model.Orders;
import pl.agh.tons.project.model.User;

public class OrdersServiceImpl implements OrdersService {

    private OrdersDao ordersDao;

    private BookForSaleDao bookForSaleDao;

    private UserDao userDao;

    @Inject
    public OrdersServiceImpl(OrdersDao ordersDao, BookForSaleDao bookForSaleDao, UserDao userDao) {
        this.ordersDao = ordersDao;
        this.bookForSaleDao = bookForSaleDao;
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public void orderBook(int userId, int bookId) {
        User user = userDao.getById(userId);
        BookForSale bookForSale = bookForSaleDao.getById(bookId);
        Orders order = new Orders(user, bookForSale);

        ordersDao.addOrders(order);
    }

    @Override
    @Transactional
    public void cancelOrder(int userId, int bookId) {
        Orders order = ordersDao.getOrders(userId, bookId);

        ordersDao.removeOrders(order);
    }
}
