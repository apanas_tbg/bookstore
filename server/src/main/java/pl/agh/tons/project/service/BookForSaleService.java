package pl.agh.tons.project.service;

import pl.agh.tons.project.model.BookForSale;

import java.util.List;

public interface BookForSaleService {

    List<BookForSale> getAll();

    void registerBookForSale(BookForSale book);

    void removeBookForSale(BookForSale book);

    BookForSale findBookForSale(String parameter, String value);
}
