package pl.agh.tons.project.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import pl.agh.tons.project.dao.abstraction.BookForSaleDao;
import pl.agh.tons.project.dao.abstraction.LoanDao;
import pl.agh.tons.project.dao.abstraction.UserDao;
import pl.agh.tons.project.model.BookForSale;

import java.util.List;

public class BookForSaleServiceImpl implements BookForSaleService {

    private UserDao userDao;
    private BookForSaleDao bookForSaleDao;

    private Gson gson = new Gson();

    @Inject
    public BookForSaleServiceImpl(UserDao userDao, BookForSaleDao bookForSaleDao) {
        this.userDao = userDao;
        this.bookForSaleDao = bookForSaleDao;
    }

    @Override
    public List<BookForSale> getAll() {
        return bookForSaleDao.getAll();
    }

    @Override
    public void registerBookForSale(BookForSale book) {

    }

    @Override
    public void removeBookForSale(BookForSale book) {

    }

    @Override
    public BookForSale findBookForSale(String parameter, String value) {
        return null;
    }
}
