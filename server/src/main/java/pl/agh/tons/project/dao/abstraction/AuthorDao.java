package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Author;

public interface AuthorDao extends Dao<Author> {
}
