package pl.agh.tons.project.dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import pl.agh.tons.project.dao.abstraction.AbstractDao;
import pl.agh.tons.project.dao.abstraction.BoughtDao;
import pl.agh.tons.project.model.Bought;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class BoughtDaoImpl extends AbstractDao<Bought> implements BoughtDao {

    @Inject
    public BoughtDaoImpl(Provider<EntityManager> entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public void addBought(Bought bought) {
        entityManagerFactory.get().persist(bought);
    }

    @Override
    public void setBought(Bought bought) {
        entityManagerFactory.get().merge(bought);
    }

    @Override
    public Bought getBought(int copyId, int userId) {
        Query query = entityManagerFactory.get().createQuery("from Bought WHERE copyforsale.id = :copyId AND " +
                "user.id = :userId");
        query.setParameter("copyId", copyId);
        query.setParameter("userId", userId);

        return (Bought) query.getResultList().get(0);
    }

    @Override
    public List<Bought> getByForeignKey(String column, int id) {

        Query query =  entityManagerFactory.get().createQuery("from "+ clazz.getSimpleName() +
                " WHERE "+column+" = :"+column+" AND archive = 0");
        query.setParameter(column, id);
        List<Bought> list = (List<Bought>) query.getResultList();

        return list;
    }
}
