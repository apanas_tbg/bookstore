package pl.agh.tons.project.dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;

import pl.agh.tons.project.dao.abstraction.AbstractDao;
import pl.agh.tons.project.dao.abstraction.OrdersDao;
import pl.agh.tons.project.model.Orders;
import pl.agh.tons.project.servlet.BookForSaleServlet;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;

public class OrdersDaoImpl extends AbstractDao<Orders> implements OrdersDao {
    
    @Inject
    public OrdersDaoImpl(Provider<EntityManager> entityManagerFactory) {
        super(entityManagerFactory);
    }

    @Override
    public void addOrders(Orders orders) {
        entityManagerFactory.get().persist(orders);
        LOG.info("addOrder - user id:" + orders.toString() );
    }

    @Override
    public void removeOrders(Orders order) {
        entityManagerFactory.get().merge(order);
        entityManagerFactory.get().remove(order);
    }

    @Override
    public Orders getOrders(int userId, int bookId) {
        Query query = entityManagerFactory.get().createQuery("from Orders WHERE user_id = :userId AND" +
                " bookforsale_id = :bookId");
        query.setParameter("userId", userId);
        query.setParameter("bookId", bookId);

        return (Orders) query.getResultList().get(0);
    }
}
