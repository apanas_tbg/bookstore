package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Book;

public interface BookDao extends Dao<Book> {
}
