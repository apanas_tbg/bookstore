package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Bought;

public interface BoughtDao extends Dao<Bought> {
    void addBought(Bought bought);

    void setBought(Bought bought);

    Bought getBought(int copyId, int userId);
}
