package pl.agh.tons.project.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import pl.agh.tons.project.model.BookForSale;
import pl.agh.tons.project.service.BookForSaleService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Singleton
public class BookForSaleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BookForSaleServlet.class);

    private BookForSaleService bookForSaleService;
    private WebProtocol webProtocol;

    @Inject
    public BookForSaleServlet(BookForSaleService bookForSaleService, WebProtocol webProtocol) {
        this.bookForSaleService = bookForSaleService;
        this.webProtocol = webProtocol;
    }

    protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws IOException, ServletException {

        httpResponse.setContentType("application/json");
        httpResponse.setCharacterEncoding("UTF-8");

        List<BookForSale> booksForSale = bookForSaleService.getAll();

        Response<BookForSale> response = new Response(booksForSale);
        response.setMsg("Wszystkie dostepne ksiazki w sklepie.");
        response.setSuccess(true);
        httpResponse.getWriter().write(webProtocol.prepareResponse(response));
    }
}
