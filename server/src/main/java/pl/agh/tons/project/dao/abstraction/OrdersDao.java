package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Orders;

public interface OrdersDao extends Dao<Orders> {

    void addOrders(Orders orders);

    void removeOrders(Orders orders);

    Orders getOrders(int userId, int bookId);
}
