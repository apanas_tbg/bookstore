package pl.agh.tons.project.service;

public interface ReservationService {

    void reserveBook(int userId, int bookId);

    void cancelReservation(int userId, int bookId);
}
