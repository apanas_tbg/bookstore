package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Fine;

public interface FineDao extends Dao<Fine> {
}
