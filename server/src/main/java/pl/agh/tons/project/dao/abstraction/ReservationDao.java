package pl.agh.tons.project.dao.abstraction;

import pl.agh.tons.project.model.Reservation;

public interface ReservationDao extends Dao<Reservation> {

    void addReservation(Reservation reservation);

    void removeReservation(Reservation reservation);

    Reservation getReservation(int userId, int bookId);
}
