package pl.agh.tons.project.dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import pl.agh.tons.project.dao.abstraction.AbstractDao;
import pl.agh.tons.project.dao.abstraction.BookForSaleDao;
import pl.agh.tons.project.model.BookForSale;

import javax.persistence.EntityManager;

public class BookForSaleDaoImpl extends AbstractDao<BookForSale> implements BookForSaleDao {

    @Inject
    public BookForSaleDaoImpl(Provider<EntityManager> entityManagerFactory) {
        super(entityManagerFactory);
    }
}
