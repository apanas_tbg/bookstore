package pl.agh.tons.project.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import pl.agh.tons.project.model.Bought;
import pl.agh.tons.project.model.User;
import pl.agh.tons.project.service.BoughtService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Singleton
public class BoughtServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(BoughtServlet.class);

    private BoughtService boughtService;
    private WebProtocol webProtocol;

    @Inject
    public BoughtServlet(BoughtService boughtService, WebProtocol webProtocol) {
        this.boughtService = boughtService;
        this.webProtocol = webProtocol;
    }

    protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
                                                        throws IOException, ServletException {

        httpResponse.setContentType("application/json");
        httpResponse.setCharacterEncoding("UTF-8");

        List<Bought> bought = boughtService.getBought(Integer.valueOf(httpRequest.getParameter("user_id")));

        Response<User> response = new Response(bought);
        response.setMsg("Twoje kupione ksiazki.");
        response.setSuccess(true);
        httpResponse.getWriter().write(webProtocol.prepareResponse(response));
    }

    protected void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
                                                        throws IOException, ServletException {

        Map<String, Object> requestMap = webProtocol.prepareRequest(httpRequest);

        httpResponse.setContentType("application/json");
        httpResponse.setCharacterEncoding("UTF-8");

        int bookId = Integer.valueOf((String) requestMap.get("bookId"));
        int userId = ((Double) requestMap.get("userId")).intValue();

        boolean rented = boughtService.buyBook(bookId, userId);

        Response response = new Response();
        if (rented) {
            response.setSuccess(true);
            response.setMsg("Ksiazka zostala kupiona!");
        } else {
            response.setSuccess(false);
            response.setMsg("Brak wolnych ksiazek. Mozesz zamowic ksiazke.");
        }

        httpResponse.getWriter().write(webProtocol.prepareResponse(response));
    }
}

