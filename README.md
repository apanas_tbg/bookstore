### System obsługi księgarni i wypozyczalni online

## Architektura systemu

* Klient - Serwer

## Serwer

* Baza danych: MySQL
* Technologia: Java
* ORM: Hibernate
* Kontener wstrzykiwania zależności: Guice
* Biblioteka json: Gson

## Klient

* JavaScript - AngularJS
* HTML & CSS

## Instalacja
* Pobierz i zainstaluj VirtualBox https://www.virtualbox.org/wiki/Downloads
* Pobierz i zainstaluj Vagrant https://www.vagrantup.com/downloads.html
* Pobierz i zainstaluj dowolnego klienta Git np. https://git-scm.com/downloads
* Wykonaj Clone tego repozytorium.
* W katalogu projektu (gdzie znajduje sie Vagrantfile) wykonaj polecenie 'vagrant up' (moze byc konieczne ustawienie zmiennej VAGRANT_CWD ktora ma wskazywac na katalog projektu)
* Po instalacji strona projektu dostepna jest pod adresem http://192.168.33.10:8080/,
* Testowy dostep do aplikacji: test@test.pl/test